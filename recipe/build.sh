#!/bin/bash

# Compile epics-base
make -j$(getconf _NPROCESSORS_ONLN)

# Some file are created as read-only but they need to be modified
# by conda-build to make them relocatable
chmod u+w ${PREFIX}/base/*/linux-x86_64/*

# Create files to set/unset variables when running
# activate/deactivate
EPICS_BASE="$PREFIX/base"
EPICS_HOST_ARCH="$(startup/EpicsHostArch)"
EPICS_BASE_BIN="$EPICS_BASE/bin/$EPICS_HOST_ARCH"

# Instead of creating links we should add EPICS_BASE_BIN to the PATH
# But modifying PATH in deactivate.d doesn't work in conda 4.4.11
# This should be changed when it's fixed
cd $PREFIX/bin
for filename in caget cainfo caput camonitor caRepeater softIoc softIocPVA pvget pvput pvinfo pvlist
do
  ln -s ../base/bin/$EPICS_HOST_ARCH/$filename $filename
done

mkdir -p $PREFIX/etc/conda/activate.d
cat <<EOF > $PREFIX/etc/conda/activate.d/epics_vars.sh
export PYEPICS_LIBCA="${EPICS_BASE}/lib/${EPICS_HOST_ARCH}/libca.so"
export EPICS_BASE="$EPICS_BASE"
export EPICS_HOST_ARCH="$EPICS_HOST_ARCH"
export EPICS_BASE_BIN="$EPICS_BASE_BIN"
EOF

mkdir -p $PREFIX/etc/conda/deactivate.d
cat <<EOF > $PREFIX/etc/conda/deactivate.d/epics_vars.sh
unset PYEPICS_LIBCA
unset EPICS_BASE
unset EPICS_HOST_ARCH
unset EPICS_BASE_BIN
EOF
